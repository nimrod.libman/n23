use <pcb.scad>;
use <vertically_rounded_cube.scad>;

pcb_thickness = 1;
pcb_thickness_tol = .1;
$fn = $preview ? 16 : 32;



module left_upper_plate(cutter)
{
    render() difference()
    {
        union()
        {
            translate([ -63.5, 18 - 22, 0 ]) vertically_rounded_cube([ 31, 22, 2 ], 1);
            translate([ -53, -4 - 8.5, 0 ]) vertically_rounded_cube([ 10.5, 8.5, 2 ], 1);
            translate([ -44.5, -2 - 7.5, 0 ]) vertically_rounded_cube([ 12, 7.5, 2 ], 1);

            translate([ -53, -4 - 1, 0 ]) cube([ 1, 1, 2 ]);
            translate([ -53 - 1, -4 - 1, 0 ]) rotate([ 0, 0, 0 ]) corner_cutout(r = 1, h = 2);

            translate([ -42.5 + 1, -9.5 - 1, 0 ]) rotate([ 0, 0, 90 ]) corner_cutout(r = 1, h = 2);

            // The poles because we got to have that!
            translate([ -34, 15, -pcb_thickness - pcb_thickness_tol - 2 ])
                cylinder(r = 1.1, h = 2 + pcb_thickness + pcb_thickness_tol);

            translate([ -34, -6, -pcb_thickness - pcb_thickness_tol - 2 ])
                cylinder(r = 1.1, h = 2 + pcb_thickness + pcb_thickness_tol);
        }
        if (!cutter)
        {
            for (i = face_button_positions())
            {
                translate([ 0, 0, -2 ]) translate(i) cylinder(r = 4, h = 10);
            }
        }

        for (i = outer_hole_positions())
        {
            translate([ 0, 0, -2 ]) translate(i) cylinder(r = 1.27, h = 10);
        }

        // pcb();
    }
}

module right_upper_plate(cutter)
{
    mirror([ 1, 0, 0 ]) left_upper_plate(cutter);
}

module left_lower_plate(cutter)
{
    // Top right = -16,22
    // width = 11.75
    // height = 11.5
    render() difference()
    {
        union()
        {
            translate([ -16 - 11.75, -22 - 11.5, 0 ]) vertically_rounded_cube([ 11.75, 11.5, 2 ], 1);
            translate([ -33.5, -33.5, 0 ]) vertically_rounded_cube([ 5.75 + 2, 6.5, 2 ], 1);

            translate([ -28.75, -26, 0 ]) rotate([ 0, 0, -90 ]) corner_cutout(r = 1, h = 2);
        }
        if (!cutter)
        {
            for (i = face_button_positions())
            {
                translate([ 0, 0, -2 ]) translate(i) cylinder(r = 4, h = 10);
            }
        }

        for (i = outer_hole_positions())
        {
            translate([ 0, 0, -2 ]) translate(i) cylinder(r = 1.27, h = 10);
        }
    }
}

module right_lower_plate(cutter)
{
    mirror([ 1, 0, 0 ]) left_lower_plate(cutter);
}

color("purple") left_upper_plate(cutter = false);
color("purple") left_lower_plate(cutter = false);
color("orange") right_upper_plate(cutter = true);
color("orange") right_lower_plate(cutter = true);
pcb(1, .1);