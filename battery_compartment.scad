use <front_plates.scad>;
use <pcb.scad>;
use <screws.scad>
use <vertically_rounded_cube.scad>;

$fn = $preview ? 16 : 32;
compartment_height = 6.2;
module battery_compartment(compartment_height, pcb_thickness, pcb_thickness_tol,
                           cover_thickness) {
  render() difference() {
    translate([ 0, 0, -pcb_thickness - pcb_thickness_tol ]) union() {
      difference() {
        union() {
          translate([ -32, -20, -compartment_height ])
              vertically_rounded_cube([ 64, 40, compartment_height ], 2.5);

          translate([ -36, -8, -4 ]) vertically_rounded_cube([ 4, 28, 4 ], 2);

          translate([ -34, -10, -4 ]) cube([ 5, 30, 4 ]);

          translate([ 36 - 4, -8, -compartment_height ])
              vertically_rounded_cube([ 4, 28, compartment_height ], 2);

          translate([ 34 - 6, -8, -compartment_height ])
              cube([ 6, 28, compartment_height ]);

          translate([ 34, -10, -compartment_height ]) rotate([ 0, 0, 90 ])
              corner_cutout(r = 2, h = compartment_height);

          translate([ 40 - 8, -8, -compartment_height ])
              vertically_rounded_cube([ 8, 18, compartment_height ], 2);
          translate([ 38, 12, -compartment_height ]) rotate([ 0, 0, 180 ])
              corner_cutout(r = 2, h = compartment_height);
        }
        translate([ 34, -10, -6 ]) cylinder(r = 2, h = compartment_height + 2);
        translate([ -34, -10, -6 ]) cylinder(r = 2, h = compartment_height + 2);

        left_upper_plate(cutter = true);
        left_lower_plate(cutter = true);
        right_upper_plate(cutter = true);
        right_lower_plate(cutter = true);
        translate([ 0, 0, 5 ]) cube([ 59, 35, 112 ], center = true);

        // cutout to accomodate the stylus
        translate([ -35, -21.25, -compartment_height - 1.5 ])
            cube([ 59, 3, compartment_height ]);

        // cutout for the battery plate screw insert - slightly deeper than the
        // actual insert
        translate([ 35, 0, -compartment_height ]) cylinder(h = 5, d = 3.5);

        translate([ 0, 0, -compartment_height + 0.1 ])
            battery_cover_cutter(2, 0.1);
      }
      // shelf to hold the battery
      // http://www.cpkb.org/wiki/Nokia_BL-5C_battery suggests max thickness is
      // 6mm.#

      translate([ -59 / 2, -35 / 2, -5 + 6 / 2 ]) cube([ 10, 10, 2 ]);
      translate([ 59 / 2 - 5, -35 / 2, -5 + 6 / 2 ]) cube([ 5, 5, 2 ]);
    }
    translate([ -30 + 0.5, -18 + 0.5, -10 ]) rotate([ 0, 0, 180 ])
        corner_cutout(h = 10, r = 2.5);
    translate([ -35, -21.5, -compartment_height - 1.5 ]) cube([ 59, 4, 10 ]);

    translate([ -3 - 59 / 2, -1.5 - 7, -compartment_height - 2 / 2 - 1 ])
        cube([ 6, 2, 3 ]);

    translate([ -3 - 59 / 2, -1.5 + 7, -compartment_height - 2 / 2 - 1 ])
        cube([ 6, 2, 3 ]);
  }
}

module battery_cover_cutter(thickness, extra) {
  cover_width = 59;
  cover_height = 37;
  render() translate(
      [ -(cover_width) / 2, -(cover_height) / 2, -thickness ]) union() {
    translate([ -extra, -extra, -extra ]) vertically_rounded_cube(
        [
          cover_width + extra * 2, cover_height + extra * 2, thickness + extra
        ],
        2);

    // screw area
    translate([
      cover_width - 10, (cover_height - extra * 2) / 2 - 10 / 2, 0 - extra
    ])
        vertically_rounded_cube(
            [ 20 + extra, 10 + extra * 2, thickness + extra ], 2);

    // fingers to pry under cover on the other side
    translate([ -3 + 2, (cover_height - extra * 2) / 2 - 1.5 - 7, thickness ])
        cube([ 6, 2 + extra * 2, 2 + extra ]);
    translate(
        [ -3 - extra, (cover_height + extra * 2) / 2 - 1.5 - 7, thickness ])
        translate([ 3, 1, 1 ]) rotate([ 90, 0, 0 ]) translate([ -3, -1, -1 ])
            vertically_rounded_cube(
                dims = [ 6 + extra, 2 + extra, 2 + extra * 2 ], radius = 1);

    translate([ -3 + 2, (cover_height - extra * 2) / 2 - 1.5 + 7, thickness ])
        cube([ 6, 2 + extra * 2, 2 + extra ]);
    translate(
        [ -3 - extra, (cover_height + extra * 2) / 2 - 1.5 + 7, thickness ])
        translate([ 3, 1, 1 ]) rotate([ 90, 0, 0 ]) translate([ -3, -1, -1 ])
            vertically_rounded_cube(
                dims = [ 6 + extra, 2 + extra, 2 + extra * 2 ], radius = 1);
  }
}

module battery_cover(thickness){
    render() difference(){battery_cover_cutter(thickness, 0);
translate([ 35, 0, -thickness + 0.5 ]) m2_bolt(l = 5, extension = 2);
}
}

// pcb(1,0.1);
*battery_compartment(compartment_height, 1, 0.1, 2);

#render() battery_cover_cutter(thickness = 2, extra = 0.1);
color("green") render() battery_cover(2);