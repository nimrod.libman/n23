module vertically_rounded_cube(dims, radius, center = false)
{
    if (center)
    {
        translate([ -dims[0] / 2, -dims[1] / 2, -dims[2] / 2 ]) __vertically_rounded_cube_inner(dims, radius);
    }
    else
    {
        __vertically_rounded_cube_inner(dims, radius);
    }
}

module __vertically_rounded_cube_inner(dims, radius)
{
    hull()
    {
        translate([ radius, radius, 0 ]) cylinder(r = radius, h = dims[2]);
        translate([ dims[0] - radius, radius, 0 ]) cylinder(r = radius, h = dims[2]);
        translate([ dims[0] - radius, dims[1] - radius, 0 ]) cylinder(r = radius, h = dims[2]);
        translate([ radius, dims[1] - radius, 0 ]) cylinder(r = radius, h = dims[2]);
    }
}


module corner_cutout(r, h)
{
    difference()
    {
        cube([ r, r, h ]);
        translate([ 0, 0, -h / 2 ]) cylinder(r = r, h = h * 2);
    }
}

module quarter_circle(r, h){
    scale([r,r,h])
    difference() {
        cylinder(h = 1, r = 1);
        translate([0,-1,0]) cube([1,2,1+0.1]);
         translate([-1,0,0])cube([2,1,1+0.1]);
    }
}