use<battery_compartment.scad>;
use<buttons.scad>;
use<front_plates.scad>;
use<pcb.scad>;
use<torus.scad>;
use<vertically_rounded_cube.scad>;
$fn = $preview ? 16 : 256;

stylus_height = 8;
stylus_diameter = 5.3;

case_width = 134;
case_height = 68;
case_lower_thickness = 10;
case_upper_thickness = 7;
case_radius = stylus_height - stylus_diameter;

cutout_width = 74;
cutout_height = 46;
cutout_thickness = 7.6;
cutout_upper_thickness = 3;

pcb_thickness = 1;
pcb_thickness_tol = 0.1;

compartment_height = cutout_thickness - pcb_thickness - pcb_thickness_tol;
screen_bevel_radius = 1.5;

module cutout_stylus() {
  // TODO tolerancing?
  length = 90;
  head_len = 11;
  notch_faregde = 8;
  notch_width = 0.8;
  notch_length = 2;
  translate([ -case_width / 2, -case_height / 2 + 13.5, -case_lower_thickness ])
      rotate([ 180, 90, 180 ])
          translate([ -stylus_height + stylus_diameter / 2, 0, 0 ])
              render() union() {
    cylinder(length, d = stylus_diameter);

    difference() {
      translate([ stylus_diameter / 2, stylus_diameter / 2, case_radius ])
          rotate([ 90, 0, 0 ]) cylinder(h = stylus_diameter, r = case_radius);
      translate([ -5, -5, 0 ]) cube([ stylus_diameter / 2 + 5, 10, 10 ]);
    }
    translate([ 0, -stylus_diameter / 2, 0 ])
        cube([ stylus_diameter / 2, stylus_diameter, case_radius ]);
    translate([ 0, -stylus_diameter / 2, case_radius ]) cube([
      stylus_diameter / 2 + case_radius, stylus_diameter, head_len - case_radius
    ]);

    translate([
      -stylus_diameter / 2 - notch_length / 2, notch_width / 2,
      notch_faregde - notch_length / 2
    ]) scale([ 0.75, 1, 1 ]) rotate([ 90, 0, 0 ])
        cube([ notch_length, notch_length, notch_width ]);
  }
}

module cutout_shoulder_buttons(extended = false) {
  // Slots for shoulder buttons
  render() union() for (j = [ -58, -41, 41, 58 ]) {
    translate([ j, 27.4, -pcb_thickness - pcb_thickness_tol ]) if (extended) {
      shoulder_buttoncap_cutter(pusher_length = 10, inner_length = 14);
    }
    else {

      shoulder_buttoncap_cutter(pusher_length = 10, inner_length = 10.5);
    }
  }
}

module cutout_usb_slot() {
  translate([ 0, 33, -pcb_thickness - pcb_thickness_tol ]) scale([ 1, 2, 1 ])
      rotate([ 0, 180, 0 ]) usbc_port();
}

module cutout_reset_button() {

  translate([ -13, 26.7, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) clicky();
  for (i = [0:0.1:1.5]) {
    translate([ -13, 26.7 - i, -pcb_thickness - pcb_thickness_tol ])
        rotate([ 0, 180, 0 ]) scale([ 1.05, 1.05, 1.05 ]) reset_buttoncap(7);
  }
}

module cutout_volume_buttons() {
  for (pos = [ [ 10, -32.35, 0 ], [ -10, -32.35, 0 ] ]) {
    translate([ 0, 0, -pcb_thickness - pcb_thickness_tol ]) translate(pos)
        rotate([ 0, 180, 180 ]) clicky();

    for (i = [0:0.2:1.2]) {
      translate([ 0, i, -pcb_thickness - pcb_thickness_tol ]) translate(pos)
          rotate([ 0, 180, 180 ]) volume_buttoncap_cutter(3, 1.0, 2, 1.05);
    }
  }
}

module cutout_power_cover() {
  for (i = [-0.5:0.5:2.5]) {
    translate([ 58.7, -i - 13.9 + 8.8, -pcb_thickness - pcb_thickness_tol ])
        rotate([ 0, 180, 180 ]) switch_cap(1.05);
  }
}

module cutout_socket_joiners() {

  translate([
    9.19 / 2, case_height / 2 - 2.5 - 2.5,
    -(1.705 + pcb_thickness + pcb_thickness_tol)
  ]) cube([ 7 - 9.19 / 2 + 2, 2.5, 1.705 + pcb_thickness + pcb_thickness_tol ]);

  translate([
    -9.19 / 2, case_height / 2 - 2.5,
    -(1.705 + pcb_thickness + pcb_thickness_tol)
  ]) rotate([ 0, 0, 180 ])
      cube([ 7 - 9.19 / 2, 2.5, 1.705 + pcb_thickness + pcb_thickness_tol ]);

  translate([
    -((9 - 1.5 * 1.05 / 2)) - 7, case_height / 2 - 2.5 - 2.5,
    -((0.91 + 0.75) / 2 + pcb_thickness + pcb_thickness_tol)
  ])
      cube([
        (9 - 1.5 * 1.05 / 2), 2.5, (0.91 + 0.75) / 2 + pcb_thickness +
        pcb_thickness_tol
      ]);

  translate([
    -12.5, -case_height / 2 - 2.5,
    -((0.91 + 0.75) / 2 + pcb_thickness + pcb_thickness_tol)
  ]) cube([ 25, 2.5, (0.91 + 0.75) / 2 + pcb_thickness + pcb_thickness_tol ]);
  translate(
      [ -5, -case_height / 2 - 2.5, -2.25 - pcb_thickness - pcb_thickness_tol ])
      cube([ 10, 2.5, 2.55 + pcb_thickness - pcb_thickness_tol ]);
}

module cutout_slot_countersinks() {
  translate([
    0, case_height / 2 - 2.5 - 1.5 + 0.75,
    -pcb_thickness - pcb_thickness_tol - (3.16 + 0.15) / 2
  ]) rotate([ -90, 90, 0 ]) hull() {
    vertically_rounded_cube([ 4.8, 11.2, 1.5 ], radius = 0.8, center = true);
    translate([ 0, 0, 1.5 ])
        vertically_rounded_cube([ 6, 14, 1.5 ], radius = 1, center = true);
  }
}

module cutout_countersunk_m2_path() {
  render() union() {
    cylinder(r = 2.5 / 2, h = 25, center = true);

    translate([ 0, 0, -case_lower_thickness ])
        cylinder(h = case_lower_thickness * 1 / 2 - 3, r = 2.1);
    translate([ 0, 0, -case_lower_thickness / 2 - 3 ])
        cylinder(h = 2, r1 = 2.1, r2 = 0);

    translate([ 0, 0, pcb_thickness - pcb_thickness_tol ])
        cube([ 4.20, 4.20, 4 ], center = true);
  }
}

module trim_circle(d_in, d_out, height) {
  render() difference() {
    cylinder(h = height, r = d_out / 2);
    cylinder(h = height, r = d_in / 2);
  }
}

module rear_case(compartment_height, pcb_thickness, pcb_thickness_tol) {
  render() difference() {
    render() union() {
      render() translate([
        -case_width / 2, -case_height / 2 - 2.5, -case_lower_thickness +
        case_radius
      ]) difference() {
        hull() {
          vertically_rounded_cube(
              [ case_width, case_height, case_lower_thickness - case_radius ],
              case_radius);
          translate([ case_width - case_radius, case_height - case_radius, 0 ])
              sphere(case_radius);
          translate([ case_radius, case_height - case_radius, 0 ])
              sphere(case_radius);
          translate([ case_width - case_radius, case_radius, 0 ])
              sphere(case_radius);
          translate([ case_radius, case_radius, 0 ]) sphere(case_radius);
        };

        translate([
          (case_width - cutout_width) / 2, (case_height - cutout_height) / 2,
          case_lower_thickness - case_radius -
          cutout_thickness
        ]) cube([ cutout_width, cutout_height, cutout_thickness ]);

        translate([
          (case_width - 24) / 2, 5, case_lower_thickness - case_radius -
          cutout_thickness
        ]) cube([ 24, 10, cutout_thickness ]);
        translate([
          (case_width - 34) / 2, 8.5, case_lower_thickness - case_radius -
          cutout_thickness
        ]) cube([ 34, 10, cutout_thickness ]);
      }
      // Supports for the joysticks

      translate([ -42, -19, 0 ]) translate([ -5, -5, -cutout_thickness ]) cube([
        10, 10, cutout_thickness - pcb_thickness - pcb_thickness_tol - 0.7
      ]);
      translate([ 42, -19, 0 ]) translate([ -5, -5, -cutout_thickness ]) cube([
        10, 10, cutout_thickness - pcb_thickness - pcb_thickness_tol - 0.7
      ]);
    }

    render() cutout_shoulder_buttons(extended = true);

    // Slot for 3.5mm jack
    translate([ 3.2, -34.725, -pcb_thickness - pcb_thickness_tol ])
        rotate([ 0, 180, 0 ]) audio_jack();
    translate([
      0, -(case_height / 2), 10 - 2.25 - pcb_thickness -
      pcb_thickness_tol
    ]) cube([ 5.1, (case_width - cutout_width) / 2, 20 ], center = true);

    battery_compartment(compartment_height, pcb_thickness, pcb_thickness_tol);
    translate([ 0, 0, 1 ]) battery_compartment(
        compartment_height, pcb_thickness, pcb_thickness_tol);

    render() pcb(pcb_thickness, pcb_thickness_tol);

    // slot for USB
    cutout_usb_slot();
    translate([
      -9.19 / 2, (cutout_height / 2) - 3,
      -pcb_thickness - pcb_thickness_tol - 3.36 / 2
    ]) cube([ 9.19, (case_height - cutout_height) + 2 / 2, 10 ]);

    // volume buttons, slots for them
    render() union() for (j = [0:0.1:2]) {

      translate([ 0, 0, j ]) cutout_volume_buttons();
    }

    // Reset button
    render() union() for (j = [0:0.1:2]) {
      translate([ 0, 0, j ]) cutout_reset_button();
    }

    render() union() for (j = [0:1:6]) {
      for (i = [0:1:3]) {
        translate(
            [ j + 58.7, -13.9 + 8.8, i - pcb_thickness - pcb_thickness_tol ])
            rotate([ 0, 180, 180 ]) cube([ 4.4, 8.7, 4.4 ]);
      }
    }
    render() union() for (j = [0:0.5:10]) {
      translate([ 0, 0, j ]) cutout_power_cover();
    }

    for (pos = outer_hole_positions()) {
      translate(pos) cutout_countersunk_m2_path();
    }

    render() cutout_socket_joiners();

    // countersink USB slot
    render() cutout_slot_countersinks();

    render() cutout_stylus();

    translate([
      -case_width / 2, -case_height / 2 + 13.5 - stylus_diameter / 2, -
      case_lower_thickness
    ]) cube([ stylus_diameter, stylus_diameter, stylus_diameter ]);

    render() translate([ 0, 0, -cutout_thickness + 0.1 ])
        battery_cover_cutter(case_lower_thickness - cutout_thickness, 0.1);

    render() translate([ 0, 0, -cutout_thickness ])
        battery_cover_cutter(case_lower_thickness - cutout_thickness, 0.1);

    translate([ 0, -32, 0 ]) cube([ 11, 6, 5.75 ], center = true);

    translate([ 59.8, -29, 0 ]) cube([ 1, 2, 5.67 ], center = true);

    // cutout to make accessing battery compartment easier
    translate([ 40, 0, -case_lower_thickness - 0.4 ]) rotate([ 0, -75, 0 ])
        cube([ 2, 10.2, 10 ], center = true);

    // Cutout to make accessing stylus easier
    translate([
      -case_width / 2 + 10, -case_height / 2 + 13.5, -case_lower_thickness - 2
    ]) rotate([ 0, 15, 0 ]) cube([ 20, stylus_diameter, 6 ], center = true);
  }
}

module front_case(compartment_height) {
  render() difference() {
    render() union() {
      render() translate([ -case_width / 2, -case_height / 2 - 2.5, 0 ])
          hull() {
        vertically_rounded_cube(
            [ case_width, case_height, case_upper_thickness - case_radius ],
            case_radius);
        translate([
          case_width - case_radius, case_height - case_radius,
          case_upper_thickness -
          case_radius
        ]) hemisphere(case_radius);
        translate([
          case_radius, case_height - case_radius, case_upper_thickness -
          case_radius
        ]) hemisphere(case_radius);
        translate([
          case_width - case_radius, case_radius, case_upper_thickness -
          case_radius
        ]) hemisphere(case_radius);
        translate(
            [ case_radius, case_radius, case_upper_thickness - case_radius ])
            hemisphere(case_radius);
      };

      // Close the cutouts for the USB
      translate([ -9.19 / 2, case_height / 2 - 2.5 - 2.5, -5 ])
          cube([ 9.19, 2.5, 5 ]);

      // Close the cutout for the 3.5mm jack and the volume switches.
      translate([ -20 / 2, -case_height / 2 - 2.5, -5 ]) cube([ 20, 1.5, 5 ]);

      // Close the cutout for reset switch
      translate([ 7, case_height / 2 - 2.5 - 2.5, -5 ]) cube([ 3, 2.5, 5 ]);

      // cover over the power switch
      translate([ case_width / 2 - 3.2, -13.9 + 8.8 - 10, -1.5 ])
          cube([ 3.2, 10, 1.5 ]);

      // Areas between the slot covers to ensure there is enough material to
      // print (i.e no points on the curves)
      cutout_socket_joiners();

      // Add M2 poles to slot into the rear case
      for (pos = outer_hole_positions()) {
        translate([ 0, 0, -6 ]) translate(pos) cylinder(h = 6, r = 2 / 2);
      }
    }

    pcb(pcb_thickness, pcb_thickness_tol);
    render() left_upper_plate(cutter = true);
    render() left_lower_plate(cutter = true);
    render() right_upper_plate(cutter = true);
    render() right_lower_plate(cutter = true);

    for (p = joystick_positions()) {
      translate(p) cylinder(d = 15.5, h = 20);
    }

    // screen area cutout (tolerances guesstimted by rounding to convenient
    // whole numbers)
    translate([ -50 / 2, -37 / 2 + 1.2, 0 ])
        vertically_rounded_cube(dims = [ 50, 37, 20 ], radius = 1);

    translate([
      -50 / 2 - screen_bevel_radius, -37 / 2 + 1.2 - screen_bevel_radius,
      case_upper_thickness -
      screen_bevel_radius
    ]) vertically_rounded_cube(dims =
                                   [
                                     50 + screen_bevel_radius * 2,
                                     37 + screen_bevel_radius * 2,
                                     screen_bevel_radius
                                   ],
                               radius = 1 + screen_bevel_radius);
    cutout_shoulder_buttons();

    // face buttons
    render() for (pos = face_button_positions()) {
      translate([ 0, 0, 2.5 ]) translate(pos) face_buttoncap_cutter();

      translate([ 0, 0, 1 ]) translate(pos) face_buttoncap_cutter();

      translate([ 0, 0, -1 ]) translate(pos) face_buttoncap_cutter();
      translate([ 0, 0, -2 ]) translate(pos) face_buttoncap_cutter();
    }

    // light guides
    for (pos = led_positions()) {
      translate(pos) translate([ -3 / 2, -3.5 / 2, 0 ]) cube([ 3, 3.5, 2 ]);
      translate(pos) cylinder(d = 3, h = 20);
    }

    render() rear_case(compartment_height, pcb_thickness, pcb_thickness_tol);
    render() cutout_usb_slot();
    render() cutout_reset_button();
    render() cutout_volume_buttons();
    render() cutout_slot_countersinks();
    render() cutout_stylus();

    // Slot for the SWD header
    for (i = [0:1:5]) {
      translate([ -59.79, -12.475, i ]) debug_header();
    }

    // 1.7mm holes for the screw to eat into
    render() for (i = [
                    [ -31, -30, 0 ], [ 31, -30, 0 ], [ -60.5, 9.5, 0 ],
                    [ 60.5, 9.5, 0 ]
                  ]) {
      translate([ 0, 0, -3 ]) translate(i)
          trim_circle(d_in = 2, d_out = 3, height = 5);

      translate([ -1.5, -1.5, -1 ]) translate(i) cube([ 3, 3, 3 ]);
    }

    render() for (i = outer_hole_positions()) {
      translate([ -1.5, -1.5, -10 ]) translate(i) cube([ 3, 3, 10 ]);
      translate(i) cylinder(r = 1.7 / 2, h = 4);
    }

    // Last minute cleanups
    translate([ 46, -15.45, 0 ]) cube([ 11, 2, 3.65 ]);
    translate([ 43, -15.45, 0 ]) cube([ 10, 6, 3.65 ]);
    translate([ -38, -15.45, 0 ]) cube([ 2, 2, 3.65 ]);
    translate([ -54, -17, -1 ]) cube([ 14, 8, 3 ]);
  }

  // bevel around screen

  translate(
      [ -50 / 2, -37 / 2 + 1.2, case_upper_thickness - screen_bevel_radius ])
      difference() {
    union() {
      translate([ 50 - 1, 37 - 1, 0 ]) rotate_extrude(angle = 90, convexity = 1)
          translate([ screen_bevel_radius + 1, 0 ])
              circle(r = screen_bevel_radius);

      translate([ 1, 37 - 1, 0 ]) rotate([ 0, 0, 90 ])
          rotate_extrude(angle = 90, convexity = 1)
              translate([ screen_bevel_radius + 1, 0 ])
                  circle(r = screen_bevel_radius);

      translate([ 1, 1, 0 ]) rotate([ 0, 0, 180 ])
          rotate_extrude(angle = 90, convexity = 1)
              translate([ screen_bevel_radius + 1, 0 ])
                  circle(r = screen_bevel_radius);

      translate([ 50 - 1, 1, 0 ]) rotate([ 0, 0, 270 ])
          rotate_extrude(angle = 90, convexity = 1)
              translate([ screen_bevel_radius + 1, 0 ])
                  circle(r = screen_bevel_radius);

      translate([ -1, -screen_bevel_radius, 0 ]) rotate([ 0, 90, 0 ])
          cylinder(h = 50, r = screen_bevel_radius);

      translate([ -1, 37 + screen_bevel_radius, 0 ]) rotate([ 0, 90, 0 ])
          cylinder(h = 50, r = screen_bevel_radius);

      translate([ -screen_bevel_radius, 0, 0 ]) rotate([ 0, 90, 90 ])
          cylinder(h = 37, r = screen_bevel_radius);

      translate([ 50 + screen_bevel_radius, 0, 0 ]) rotate([ 0, 90, 90 ])
          cylinder(h = 37, r = screen_bevel_radius);
    }

    translate([ -5, -5, -5 ]) cube([ 70, 50, 5 ]);
  }
}

module hemisphere(radius){render() difference(){sphere(radius);
translate([ -radius, -radius, -radius * 2 ])
    cube([ radius * 2, radius * 2, radius * 2 ]);
}
}

*color("white") translate([ -58, 27.4, -pcb_thickness - pcb_thickness_tol ])
    shoulder_buttoncap(7, 1, pcb_thickness, pcb_thickness_tol);

*color("white")
    translate([ -41, 27.4 - 2.5, -pcb_thickness - pcb_thickness_tol ])
        shoulder_buttoncap(7, 1, pcb_thickness, pcb_thickness_tol);

*color("white") translate([ 58, 27.4, -pcb_thickness - pcb_thickness_tol ])
    shoulder_buttoncap(7, 1, pcb_thickness, pcb_thickness_tol);

*color("white") translate([ 41, 27.4, -pcb_thickness - pcb_thickness_tol ])
    shoulder_buttoncap(7, 1, pcb_thickness, pcb_thickness_tol);

*pcb(pcb_thickness, pcb_thickness_tol);

color("purple") render()
    rear_case(compartment_height, pcb_thickness, pcb_thickness_tol);

*render() front_case(compartment_height);

*color("purple") left_upper_plate(cutter = false);
*color("purple") left_lower_plate(cutter = false);
*color("orange") right_upper_plate(cutter = false);
*color("orange") right_lower_plate(cutter = false);

*color([ 0.5, 0.5, 0.5, 1 ])
    battery_compartment(compartment_height, pcb_thickness, pcb_thickness_tol);
*color("white") translate([ 13, 26.7, -pcb_thickness - pcb_thickness_tol ])
    rotate([ 0, 180, 0 ]) reset_buttoncap(2.5);

// color("white") translate([ 0, 0, -pcb_thickness - pcb_thickness_tol ])
// translate([ 8, -32.35, 0 ])rotate([ 0, 180, 180 ]) volume_buttoncap(3, 1,
// 2, 1.00);

*color("white") translate([ 0, 0.3, -pcb_thickness - pcb_thickness_tol ])
    translate([ -10, -32.35, 0 ]) rotate([ 0, 180, 180 ])
        volume_buttoncap(3, 1, 2, 1.00);

*color("white") translate([ 0, 0.0, -pcb_thickness - pcb_thickness_tol ])
    translate([ 10, -32.35, 0 ]) rotate([ 0, 180, 180 ])
        volume_buttoncap(3, 1, 2, 1.00);

*color("white")
    translate([ 58.7, -13.9 + 8.8, -pcb_thickness - pcb_thickness_tol ])
        rotate([ 0, 180, 180 ]) switch_cap(1.00);

*color("red") for (pos = face_button_positions()) {
  translate([ 0, 0, 2.5 ]) translate(pos) face_buttoncap();
}

*color([ 0, 1, 0, .5 ]) cutout_stylus();

*color("green") translate([ 0, 0, -cutout_thickness + 0.1 / 2 ])
    battery_cover(case_lower_thickness - cutout_thickness);