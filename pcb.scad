use <vertically_rounded_cube.scad>;

$fn = $preview ? 16 : 32;

pcb_thickness = 1;
pcb_thickness_tol = 0.1;

function inner_hole_positions() = [
  [ 34, -6, 0 ], [ -34, -6, 0 ], [ 34, 15, 0 ], [ -34, 15, 0 ]
];

function outer_hole_positions() = [
  [ -24, 26, 0 ], [ 24, 26, 0 ], [ -50, 26, 0 ], [ 50, 26, 0 ],
  [ -60.5, 9.5, 0 ], [ 60.5, 9.5, 0 ], [ -60.5, -30, 0 ], [ 60.5, -30, 0 ],
  [ -31, -30, 0 ], [ 31, -30, 0 ]
];

function joystick_positions() = [ [ 47.1, -24.7 ], [ -47.1, -24.7 ] ];

function face_button_positions() = [
  [ 39.25, 1.2, 0 ],
  [ -39.25, 1.2, 0 ],
  [ 57.25, 1.2, 0 ],
  [ -57.25, 1.2, 0 ],
  [ 48.25, 10.2, 0 ],
  [ -48.25, 10.2, 0 ],
  [ 48.25, -7.8, 0 ],
  [ -48.25, -7.8, 0 ],
  [ 21.75, -28, 0 ],
  [ -21.75, -28, 0 ],
];

function speaker_positions() = [
  [ 48.250, 0, -pcb_thickness - pcb_thickness_tol - 4.5 ],
  [ -48.250, 0, -pcb_thickness - pcb_thickness_tol - 4.5 ]
];

function led_positions() = [ [ 57, -8.4, 0 ], [ 15, 26, 0 ] ];

module speaker() {
  // Based on soldering footprint to give maximum size
  color("yellow") translate([ -7.3 - 0.25, -6.5 - 0.25, 0 ])
      cube([ 14.6, 13 + 0.5, 4 + 0.5 ]);
}

module motor() {
  // Tolerances inline :)
  toleranced_height = 0.15 + 2.7 + 0.1 + 0.3;
  color("lightgrey") union() {
    translate([ 0, 0, 0 ]) cylinder(r = 5 + 0.05, h = toleranced_height);
    translate([ -1.5, -5 - 3, 0 ]) cube([ 3, 3.3, 2.65 ]);
  }
}

module big_capacitor() {
  radius = 3.15 + 0.25 + 0.25;

  color("lightblue") translate([ -radius, -radius, 0 ]) vertically_rounded_cube(
      dims = [ radius * 2, radius * 2, 5.4 + 1 ], radius = 1);
  color("grey") translate([ -radius, -radius, -3 ])
      vertically_rounded_cube(dims = [ radius * 2, radius * 2, 3 ], radius = 1);
}

module small_capacitor() {
  radius = 2 + 0.25 + 0.25;
  color("lightblue") translate([ -radius, -radius, 0 ]) vertically_rounded_cube(
      dims = [ radius * 2, radius * 2, 5 + 1.5 ], radius = 1);
  color("grey") translate([ -radius, -radius, -3 ])
      vertically_rounded_cube(dims = [ radius * 2, radius * 2, 3 ], radius = 1);
}

module clicky() {
  button_width = 1.8;
  button_width_tol = 0.1;

  button_height = 0.8;
  button_height_tol = 0.1;

  button_extension = 1.2;
  button_extension_tol = 0.1;

  body_height = 1.82;
  body_height_tol = 0.1;

  body_width = 5.2;
  body_width_tol = 0.2;

  body_depth = 2.3;
  body_depth_tol = 0.1;
  union() {
    color("lightgrey") translate(
        [ -(body_width + body_width_tol) / 2, -body_depth - body_depth_tol, 0 ])
        cube([
          body_width + body_width_tol, body_depth + body_depth_tol,
          body_height +
          body_height_tol
        ]);

    color("black") translate([
      -(button_width + button_width_tol) / 2, 0,
      -(button_height + button_height_tol) / 2 + (body_height) / 2
    ])
        cube([
          button_width + button_width_tol,
          button_extension + button_extension_tol, button_height +
          button_height_tol
        ]);
  }
}

module led() {
  width = 1.5;
  height = 1.6;
  thickness = 0.28;

  tol = 0.1;
  color("lightgrey") translate([ 0, 0, (thickness + tol) / 2 ])
      cube([ 2.1, 3.3, thickness + tol ], center = true);

  color("red") translate([ 0, 0, (0.6 + tol) / 2 ])
      cube([ 1.5, 0.8, 0.6 + tol ], center = true);
}

module audio_jack() {
  color("black") union() {
    translate([ -0.15 / 2 - 1.3, -0.15, -0 ])
        cube([ 6.4 + 0.15 + 2.6, 12 + 0.15, 4.55 + 0.15 ]);
    rotate([ 90, 0, 0 ]) translate([ 3.2, 2.25, 0 ])
        cylinder(r = 2.5 + 0.05, h = 2);
  }
}

module debug_header() {
  width = 2.54 * 3;
  width_tol = 0.2;

  thickness = 5;
  thickness_tol = 0.2;

  height = 5;
  height_tol = 0.2;

  color("black") translate(
      [ -(thickness + thickness_tol) / 2, -(width + width_tol) / 2, -3 - 0.5 ])
      cube([
        thickness + thickness_tol, width + width_tol,
        height + height_tol + 3 + 0.5
      ]);
}

module switch () {
  height = 4.7;
  width = 8.7;
  thickness = 4.4;

  all_tol = 0.2;

  bottom_width = 1.4;
  translate([ -all_tol, -all_tol, 0 ]) color("grey")
      cube([ thickness + all_tol, width + all_tol, height + all_tol ]);
  translate([ 4.4 - +all_tol / 2, 2.35 - +all_tol / 2, 1.55 - +all_tol / 2 ])
      color("black") cube([ 4 + all_tol, 2 + all_tol, 2 + all_tol ]);
  translate([ 3 - thickness / 2, -all_tol - bottom_width, -4 ]) color("grey")
      cube([ 3 + all_tol - 0.6, width + all_tol + 2 * bottom_width, 4 ]);
}

module usbc_port() {
  height = 3.16;
  height_tol = 0.15;

  height_from_base = 3.26;
  height_from_base_tol = 0.1;

  outer_width = 8.64 + 0.3;
  outer_width_tol = 0.15 + 0.1;

  length = 7.35;
  length_tol = 0.2;

  translate([ (outer_width + outer_width_tol) / 2, 0, 0 ]) color("grey")
      hull() {
    translate(
        [ 0, height_from_base + height_from_base_tol - height - height_tol, 0 ])
        rotate([ 90, 270, 0 ]) vertically_rounded_cube(
            dims =
                [
                  height + height_tol, outer_width + outer_width_tol, length +
                  length_tol
                ],
            radius = height / 2);
  }
}

module shoulder_button() {
  // This one is already toleranced B-)
  color("black") cube([ 8, 3.15, 8 ]);
  color("lightgrey") translate([ 4, 3.15, 4 ]) rotate([
    -90,
    0,
    0,
  ]) cylinder(h = 2.5, r = 3.5 / 2);
}

module joystick(pcb_thickness, pcb_thickness_tol) {

  fasteners_height = 5.2 - 3.9 + 1.2;
  union() {
    // This is kinda toleranced
    color("lightgrey") scale([ 1.05, 1.05, 1.05 ])
        translate([ 1.45 - 16.2 + 13.35 - 6.3, 7.9 - 17.2, 0 ])
            cube([ 16.95, 19, 5.2 + 0.3 ]);

    fasten_len = 19 - 17.2 + 14.9 - 3.8 / 2;

    color("lightgrey") scale([ 1.05, 1.05, 1.05 ]) translate([
      1.45 + 16.95 - 16.2 + 13.35 - 6.3, -fasten_len + 7.9 - 17.2 + 19 + 1, -2.7
    ]) cube([ 4.2, fasten_len, fasteners_height + 2.7 + 1 ]);

    // made 1mm higher so the case can accomodate it
    color("lightgrey") translate([ -6.3 + 13.35 - 3.9 / 2, 7.9 - 17.2 + 19, 0 ])
        cube([
          3.8 + 0.1 + 0.15 + 0.15, 3.9 / 2 + 1.45 + 0.15,
          5.2 - 3.95 + 0.1 + 1.2 + 0.1 + 1
        ]);
    color("lightgrey") translate(
        [ -6.3 + 13.35 - 0.15 - 16.2 - 3.9 / 2, 7.9 - 14.9 - 3.9 / 2, 0 ])
        cube([
          3.9 / 2 + 1.45 + 0.15, 3.8 + 0.1 + 0.15 + 0.15,
          5.2 - 3.95 + 0.1 + 1.2 + 0.1 + 1
        ]);

    color("black") translate([ 0, 0, 5.2 ]) cylinder(h = 3.1 + 0.3, r = 2);
  };
}

module silicone_button() {
  color("white") union() {
    cylinder(h = 1.3, r = 3.5);
    translate([ 0, 0, 1.3 ]) cylinder(r1 = 2.5, r2 = 1.8 / 2, h = 1.7);
    cylinder(h = 5, r = 1.8 / 2);
  };
}

module pcb_board(pcb_thickness, pcb_thickness_tol) {
  difference() {
    color("green")
        translate([ -63.5, -33.7, -pcb_thickness - pcb_thickness_tol ])
            cube([ 63.5 * 2, 62.7, pcb_thickness + pcb_thickness_tol ]);

    translate([ 53.5, 22, -2 ]) cube([ 10, 10, 5 ]);
    translate([ 36.5, 22, -2 ]) cube([ 9, 10, 5 ]);
    translate([ -45.5, 22, -2 ]) cube([ 9, 10, 5 ]);
    translate([ -63.5, 22, -2 ]) cube([ 10, 10, 5 ]);

    for (pos = concat(inner_hole_positions(), outer_hole_positions())) {
      translate(pos) cylinder(r = 2.2 / 2, h = 5, center = true);
    }
  }
}

module pcb(pcb_thickness, pcb_thickness_tol) {
  pcb_board(pcb_thickness, pcb_thickness_tol);

  translate([ 54, 22, -pcb_thickness - 4 - 0.25 ]) shoulder_button();
  translate([ 37, 22, -pcb_thickness - 4 - 0.25 ]) shoulder_button();
  translate([ -45, 22, -pcb_thickness - 4 - 0.25 ]) shoulder_button();
  translate([ -62, 22, -pcb_thickness - 4 - 0.25 ]) shoulder_button();

  // TODO only toleranced thickness, need W+H but can't find diagrams with
  // tolerances.
  //  Made the screen a .25mm thicker to accomodate ribbon
  // And added 1.25m to accomodate the jelly tape
  color([0.1,0.1,0.1]) translate([ -32.18, -21.36 + 1.2, 0 ])
      cube([ 60.26, 42.72, 5.25 ]);

  translate([ -47.1, -24.7, 0 ]) rotate(0)
      joystick(pcb_thickness, pcb_thickness_tol);
  translate([ 47.1, -24.7, 0 ]) rotate(0)
      joystick(pcb_thickness, pcb_thickness_tol);

  for (i = face_button_positions()) {
    translate(i) silicone_button();
  }

  translate([ 0, 30, -pcb_thickness - pcb_thickness_tol ]) rotate([ 0, 180, 0 ])
      usbc_port();

  translate([ 58.7, -13.9 + 8.8, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 180 ]) switch ();

  translate([ -59.79, -12.475, 0 ]) debug_header();

  translate([ 3.2, -34.725, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) audio_jack();

  translate([ 8.25, -26, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) big_capacitor();
  translate([ -8.25, -26, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) big_capacitor();

  translate([ 14, -25.4, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) small_capacitor();
  translate([ -14, -25.3, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) small_capacitor();

  for (pos = led_positions()) {
    translate(pos) led();
  }
  translate([ -13, 26.7, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 0 ]) clicky();

  translate([ 10, -32.35, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 180 ]) clicky();
  translate([ -10, -32.35, -pcb_thickness - pcb_thickness_tol ])
      rotate([ 0, 180, 180 ]) clicky();
}

pcb(pcb_thickness, pcb_thickness_tol);