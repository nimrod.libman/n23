module torus(inner_radius, sectional_radius) {

  rotate_extrude(angle = 360) {
    translate([ inner_radius + sectional_radius / 2, 0 ])
        circle(d = sectional_radius);
  }
}