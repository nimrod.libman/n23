







$fn = $preview ? 16 : 32;

module
countersunk_bolt (d, l, head_length)
{
  head_diam = d * 2; // Inferred from chart
  cylinder (h = l, d = d);
  cylinder (h = head_length, d1 = head_diam, d2 = d);
}
module
m2_bolt (l, extension = 0)
{
  countersunk_bolt (d = 2, l = l,
                    head_length = 1.9); // head_len grabbed from chart
  translate([0,0,-extension]) cylinder (h = extension, d = 4);
}
m2_bolt (l = 5,extension = 5);