use <pcb.scad>;
use <vertically_rounded_cube.scad>;

$fn = $preview ? 16 : 256;

face_height = 6.0;
face_rounding_radius = 1.0;
face_pusher_radius = 0.5;

mushroom_side = 4.0;
mushroom_radius = 1.0;
tol = 1.00; //[1:0.01:2]
length = 5.00;

module face_buttoncap() {
  render() difference() {
    union() {
      translate([ 0, 0, 1 ]) hull() {
        cylinder(d = 9, h = 3);

        translate([ 0, 0, face_height - face_rounding_radius ])
            scale([ 1, 1, face_rounding_radius / 4.5 ]) sphere(r = 4.5);
      }
      cylinder(h = 1, r1 = 2, r2 = 2.5);
      translate([ 0, 0, 1 ]) cylinder(d = 11.5, h = 2);
    }

    translate([ 0, 0, -2.5 ]) silicone_button();
  }
}

module face_buttoncap_cutter() {
  render() difference() {
    union() {
      translate([ 0, 0, 1 ]) union() {
        cylinder(d = 9 + 0.3 * 2, h = face_height);
        translate([ 0, 0, -0.3 - 3 ]) cylinder(d = 11.5 + 0.3 * 2, h = 2.5 + 3);
      }
      cylinder(h = 1, r1 = 2, r2 = 2.5);
    }
  }
}

module shoulder_buttoncap(length, radius, pcb_thickness, pcb_thickness_tol) {
  block_width = length - radius;
  render() difference() {
    translate([ -5, 0, -5 ]) union() {

      difference() {
        translate([ 0, -4, 10 ]) rotate([ -90, 0, 0 ])
            vertically_rounded_cube([ 10, 10, 5 ], 1);

        translate([ -10, -10, 5.25 ])
            cube([ 20, 20, pcb_thickness + pcb_thickness_tol ]);
      }

      hull() {
        translate([ 1.5, 0, 8.5 ]) rotate([ -90, 0, 0 ])
            vertically_rounded_cube([ 7, 7, block_width ], 1);
        translate([ 1.5 + 1, block_width, 8.5 - 1 ]) rotate([ 90, 0, 0 ])
            sphere(radius);
        translate([ 1.5 + 7 - 1, block_width, 8.5 - 1 ]) rotate([ 90, 0, 0 ])
            sphere(radius);

        translate([ 1.5 + 1, block_width, 2.5 ]) rotate([ 90, 0, 0 ])
            sphere(radius);
        translate([ 1.5 + 7 - 1, block_width, 2.5 ]) rotate([ 90, 0, 0 ])
            sphere(radius);
      }
    }
    translate([ 0, -4, 0 ]) cube([ 8, 8, 8 ], center = true);
    translate([ -4, -5.15, -4 ]) shoulder_button();
    translate([ -10, -20, 0.25 ])
        cube([ 20, 20, pcb_thickness + pcb_thickness_tol ]);
  }
}

module shoulder_buttoncap_cutter(pusher_length, inner_length) {
  block_width = length;
  tol = 0.3;
  inner = 7;
  outer = 10;
  render() difference() {
    translate([ -outer / 2 - tol, 0, -outer / 2 + tol ]) union() {
      translate([ (outer - inner) / 2, 0, outer - (outer - inner) / 2 ])
          rotate([ -90, 0, 0 ]) vertically_rounded_cube(
              [ inner + tol * 2, inner + tol * 2, block_width ], 1);

      translate([ 0, -inner_length + 1, 10 ]) rotate([ -90, 0, 0 ])
          vertically_rounded_cube(
              [ outer + tol * 2, outer + tol * 2, inner_length ], 1);
    }
  }
}

module reset_buttoncap(length) {
  render() translate([ -5, +1.2, 0.91 + 0.75 ]) union() {
    translate([ 0, 0, 0 ]) rotate([ -90, 0, 0 ])
        vertically_rounded_cube([ 10, 1.5, 2 ], 0.5);

    translate([ 5 - 0.75, 1, 0 ]) rotate([ -90, 0, 0 ])
        vertically_rounded_cube([ 1.5, 1.5, 3 - 1.5 ], 0.5);

    hull() {
      for (x = [ 5 - 0.75, 5 + 0.75 ]) {
        for (y = [ length, 2.5 ]) {
          for (z = [ -1.5, 0 ]) {
            translate([ x, y, z ]) rotate([ 90, 0, 0 ]) sphere(0.5);
          }
        }
      }
    }
  }
}

module volume_buttoncap(mushroom_side, mushroom_radius, length, tolerance) {
  render() union() {
    translate([ -5 * tolerance, 1.2, (0.91 + 0.75 * tolerance) ]) union() {
      translate([ 0, 0, 0 ]) scale([ tolerance, tolerance, tolerance ])
          rotate([ -90, 0, 0 ]) vertically_rounded_cube([ 10, 1.5, 1 ], 0.5);

      translate([ (5 - 0.75) * tolerance, 1 * tolerance, 0 * tolerance ])
          scale([ tolerance, tolerance, tolerance ]) rotate([ -90, 0, 0 ])
              vertically_rounded_cube([ 1.5, 1.5, length - 1.5 ], 0.5);
    }

    p = [
      (mushroom_side / 2 - mushroom_radius),
      -(mushroom_side / 2 - mushroom_radius)
    ];

    translate([ 0, mushroom_side / 2 + length, 0.91 ])
        scale([ tolerance, tolerance, tolerance ]) hull() {
      for (pos_x = p) {
        for (pos_y = p) {
          for (pos_z = p) {
            translate([ pos_x, pos_y, pos_z ]) sphere(r = mushroom_radius);
          }
        }
      }
    }
  }
}

module volume_buttoncap_cutter(mushroom_side, mushroom_radius, length,
                               tolerance) {
  render() union() {
    translate([ -5 * tolerance, 1.2, (0.91 + 0.75 * tolerance) ]) union() {
      translate([ 0, 0, 0 ]) scale([ tolerance, tolerance, tolerance ])
          rotate([ -90, 0, 0 ]) vertically_rounded_cube([ 10, 1.5, 1 ], 0.5);

      translate([ (5 - 0.75) * tolerance, 1 * tolerance, 0 * tolerance ])
          scale([ tolerance, tolerance, tolerance ]) rotate([ -90, 0, 0 ])
              vertically_rounded_cube([ 1.5, 1.5, length - 1.5 ], 0.5);
    }

    p = [
      (mushroom_side / 2 - mushroom_radius),
      -(mushroom_side / 2 - mushroom_radius)
    ];

    translate([ 0, mushroom_side / 2 + length, 0 ]) rotate([ 90, 0, 0 ])
        translate([
          -mushroom_side / 2, -mushroom_side / 2 + 0.91, -mushroom_side / 2
        ]) scale([ tolerance, tolerance, tolerance ])
            vertically_rounded_cube(
                [ mushroom_side, mushroom_side, mushroom_side ],
                radius = mushroom_radius);
  }
}

module switch_cap(s) {
  length = 2.5;
  difference() {
    translate([ 6, 3.35, 2.55 ]) union() {

      translate([ 1 / 2 * s, 0, 0 ]) rotate([ -90, 0, 90 ]) scale([ s, s, s ])
          vertically_rounded_cube([ 15, 4.5, 1 ], 0.5, center = true);

      translate([ length / 2 * s, 0, 0 ]) rotate([ -90, 0, 90 ])
          scale([ s, s, s ])
              vertically_rounded_cube([ 3, 3, length ], 0.5, center = true);

      hull() {
        translate([ length * s, (1.5 - 0.5) * s, (1.5 - 0.5) * s ])
            rotate([ -90, 0, 90 ]) scale([ s, s, s ]) sphere(0.5);

        translate([ length * s, (-1.5 + 0.5) * s, (1.5 - 0.5) * s ])
            rotate([ -90, 0, 90 ]) scale([ s, s, s ]) sphere(0.5);

        translate([ length * s, (1.5 - 0.5) * s, (-1.5 + 0.5) * s ])
            rotate([ -90, 0, 90 ]) scale([ s, s, s ]) sphere(0.5);

        translate([ length * s, (-1.5 + 0.5) * s, (-1.5 + 0.5) * s ])
            rotate([ -90, 0, 90 ]) scale([ s, s, s ]) sphere(0.5);
      }
    }

    switch ()
      ;
  }
}
// color("red") face_buttoncap();
/*
shoulder_buttoncap(7, 1, 1, 0.1);
color([ 0.8, 0, 0, 0.5 ]) shoulder_buttoncap_cutter(6, 12);
*/
// shoulder_button();

// clicky();
// intersection() {
// volume_buttoncap_cutter(mushroom_side, mushroom_radius, length, tol);
// volume_buttoncap(mushroom_side, mushroom_radius, length, tol);
// }

/*switch ()
    ;
color("white") switch_cap(tol);*/

silicone_button();
translate([ 0, 0, 2.5 ]) face_buttoncap();
#translate([ 0, 0, 2.5 ]) face_buttoncap_cutter();

/*
#translate([0,0,2])face_buttoncap();
 silicone_button();
 */
*volume_buttoncap(3, 1, 2, 1.00);

*reset_buttoncap(2.5);